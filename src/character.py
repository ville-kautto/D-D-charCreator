import random
import json
from nameGenerator import generate


def set_data(char_data):
    data = char_data[0]
    return data


class Character:
    def __init__(self, *char_data):
        # Fetching class data from external file generated by CharDataCreator.py
        self.class_data = {}
        with open('../data/class_data.json') as file:
            self.class_data = json.load(file)

        self.race_data = {}
        with open('../data/race_data.json') as file:
            self.race_data = json.load(file)
        if len(char_data) > 0:
            # print('found existing data, crating a character')
            self.data = set_data(char_data)
        else:
            self.data = {
                "speed": 0,
                "size": "null",
                "class": self.get_class(random.randint(1, 12)),
                "type": "null",
                "name": "null",
                "sex": "null",
                "race": self.get_race(random.randint(1, 9)),
                "attributes": {
                    "Strength": [],
                    "Dexterity": [],
                    "Constitution": [],
                    "Intelligence": [],
                    "Wisdom": [],
                    "Charisma": []
                },
                "features": [],
                "cantrips": [],
                "spells": [],
                "proficiencies": [],
                "health": [],
                "equipment": []
            }

            # defining class and race variables
            char_class = self.data["class"]
            char_race = self.data["race"]

            sex = random.randint(1, 2)
            if sex == 1:
                self.data['sex'] = "male"
            elif sex == 2:
                self.data['sex'] = "female"

            self.data['name'] = generate(char_race, sex)

            # printing some character data
            print('Name: {}'.format(self.data['name']))
            print('Race: {}'.format(self.data["race"].capitalize()))
            print('Class: {}'.format(char_class.capitalize()))

            self.data['size'] = self.race_data[char_race]["size"].capitalize()
            print('  Size: {!s}'.format(self.data['size']))
            self.data['speed'] = self.race_data[char_race]["speed"]
            print('  Speed: {}ft'.format(self.data['speed']))

            print("\nRacial Features:")
            self.data["features"].append(self.race_data[char_race]["features"])
            for feature in self.data["features"][0]:
                print("  {}".format(feature))

            print("\nClass Features:")
            self.data["features"].append(self.class_data[char_class]["features"])
            for feature in self.data["features"][1]:
                print("  {}".format(feature))

            # Generating, modifying and printing random attributes
            self.data["attributes"] = self.race_data[char_race]["attributes"]
            self.get_random_attr(self.data['attributes'])

            self.data['type'] = self.class_data[char_class]['type']
            if self.data['type'] == 'spellcaster':
                self.get_cantrips(self.class_data[char_class])
                self.get_spells(self.class_data[char_class])
            self.get_proficiencies(self.class_data[char_class])
            self.get_health(self.class_data[char_class])
            self.get_equipment(self.class_data[char_class])

    # Generates stats based on following model: 4d6 - smallestRoll
    # Generated stats are between 3-18, bias on median
    @staticmethod
    def generate_stat():
        random_nums = [0, 0, 0, 0]
        for i in range(0, 4):
            random_nums[i] = random.randint(1, 6)

        # Sort the list, remove the smallest number and return the sum of remaining values
        random_nums.sort()
        random_nums.pop(0)
        return sum(random_nums)

    # Defines a modifier based on given attribute
    @staticmethod
    def define_modifier(value):
        return int(value / 2 - 5)

        # Gives the character a list of attributes and modifiers
    def get_random_attr(self, attr):
        print("\nAttributes:")
        for key, value in attr.items():
            value = self.data['attributes'][key] + self.generate_stat()
            modifier = self.define_modifier(value)
            print('  {}: {}, Modifier: {:+d}'.format(key, value, modifier))
            self.data['attributes'][key] = [value, modifier]

    # Selects a class form all classes based on input
    @staticmethod
    def get_class(i):
        classes = {
            1: "barbarian",
            2: "bard",
            3: "cleric",
            4: "druid",
            5: "fighter",
            6: "monk",
            7: "paladin",
            8: "ranger",
            9: "rogue",
            10: "sorcerer",
            11: "warlock",
            12: "wizard"
        }
        return classes[i]

    def get_cantrips(self, character):
        cantrips = character["cantrips"]
        chosen_cantrips = []
        cantrip_count = character["spell_details"][0]

        print("\nCantrips:")
        for i in range(0, cantrip_count):
            random_num = random.randint(0, len(cantrips) - 1)
            print(" {}: {}".format(i + 1, cantrips[random_num]))
            chosen_cantrips.append(cantrips[random_num])
            cantrips.pop(random_num)
        self.data['cantrips'] = chosen_cantrips

    def get_spells(self, character):
        spells = character['spells']
        spell_slots = character["spell_details"][2]
        chosen_spells = []

        print("\n{}: {}".format("Spell slots", spell_slots))
        print("Spells:")
        spell_count = character["spell_details"][1]
        if spell_count > 0:
            for i in range(0, spell_count):
                random_num = random.randint(0, len(spells) - 1)
                print(" {}: {}".format(i + 1, spells[random_num]))
                chosen_spells.append(spells[random_num])
                spells.pop(random_num)
        else:
            for i in range(0, len(spells)-1):
                print(" {}: {}".format(i + 1, spells[i]))
            chosen_spells = spells
        self.data['spells'] = chosen_spells

    def get_proficiencies(self, character):
        print("\nProficiencies:")
        items = []
        for key, value in character["proficiencies"].items():
            item = "{}: {}".format(key, value)
            print("  {}".format(item))
            items.append(item)
        self.data['proficiencies'] = items

    def get_health(self, character):
        print("\nHealth:")
        items = []
        for key, value in character["health"].items():
            item = "{}: {}".format(key, value)
            print("  {}".format(item))
            items.append(item)
        self.data['health'] = items

    def get_equipment(self, character):
        items = []
        print("\nEquipment:")
        for key, value in character["equipment"].items():
            random_num = random.randint(0, len(value) - 1)
            item = "{}: {}".format(key, value[random_num])
            print("  {}".format(item))
            items.append(item)
        self.data['equipment'] = items

    # Selects a race form all races based on input
    @staticmethod
    def get_race(i):
        races = {
            1: "dragonborn",
            2: "dwarf",
            3: "elf",
            4: "gnome",
            5: "half-elf",
            6: "halfling",
            7: "half-orc",
            8: "human",
            9: "tiefling",
        }
        return races[i]

    def __str__(self):
        return '{}, {}'.format(self.data['name'], self.data['class'])

    def get_data(self):
        return self.data


