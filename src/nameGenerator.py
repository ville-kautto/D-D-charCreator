import random


sex = None


def get_dragonborn_name():
    male_names = [
        "Arjhan",
        "Balasar",
        "Bharash",
        "Donaar",
        "Ghesh",
        "Heskan",
        "Kriv",
        "Medrash",
        "Mehen",
        "Nadarr",
        "Pandjed",
        "Patrin",
        "Rhogar",
        "Shamash",
        "Shedinn",
        "Tarhun",
        "Torinn"
    ]
    female_names = [
        "Akra",
        "Biri",
        "Daar",
        "Farideh",
        "Harann",
        "Havilar",
        "Jheri",
        "Kava",
        "Korinn",
        "Mishann",
        "Nala",
        "Perra",
        "Raiann",
        "Sora",
        "Surina",
        "Thava",
        "Uadjit"
    ]

    family_name_prefix = [
        "Clethtinthiallor",
        "Daardendrian",
        "Delmirev",
        "Drachedandion",
        "Fenkenkabradon",
        "Kepeshkmolik",
        "Kerrhylon",
        "Kimbatuul",
        "Linxakasendalor",
        "Myastan",
        "Nemmonis",
        "Norixius",
        "Ophinshtalajiir",
        "Prexijandilin",
        "Shestendeliath",
        "Turnuroth",
        "Verthisathurgiesh",
        "Yarjerit"
    ]
    family_name_suffix = [
        ""
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)


def get_half_orc_name():
    name_parts = []
    if sex == "male":
        male_names = [
            "Dench",
            "Feng",
            "Gell",
            "Henk",
            "Holg",
            "Imsh",
            "Keth",
            "Krusk",
            "Mhurren",
            "Ront",
            "Shump",
            "Thokk"
        ]
        name_parts.append(male_names[random.randint(0, len(male_names)-1)])
    else:
        female_names = [
            "Baggi",
            "Emen",
            "Engong",
            "Kansif",
            "Myev",
            "Neega",
            "Ovak",
            "Ownka",
            "Shautha",
            "Sutha",
            "Vola",
            "Volen",
            "Yeveld"
        ]
        name_parts.append(female_names[random.randint(0, len(female_names)-1)])

    name = '{}'.format(name_parts[0])
    return name


def get_gnome_name():
    male_names = [
            "Alston",
            "Alvyn",
            "Boddynock",
            "Brocc",
            "Burgell",
            "Dimble",
            "Eldon",
            "Erky",
            "Fonkin",
            "Frug",
            "Gerbo",
            "Gimble",
            "Glim",
            "Jebeddo",
            "Kellen",
            "Namfoodle",
            "Orryn",
            "Roondar",
            "Seebo",
            "Sindri",
            "Warryn",
            "Wrenn",
            "Zook"
        ]
    female_names = [
            "Bimpnottin",
            "Breena",
            "Caramip",
            "Carlin",
            "Donella",
            "Duvamil",
            "Ella",
            "Ellyjobell",
            "Ellywick",
            "Lilli",
            "Loopmottin",
            "Lorilla",
            "Mardnab",
            "Nissa",
            "Nyx",
            "Oda",
            "Orla",
            "Roywyn",
            "Shamil",
            "Tana",
            "Waywocket",
            "Zanna"
        ]
    family_name_prefix = [
        "Be",
        "Dae",
        "Fol",
        "Gar",
        "Nac",
        "Mur",
        "Nin",
        "Raul",
        "Sche",
        "Tim",
        "Tu"
    ]
    family_name_suffix = [
        "kor",
        "nig",
        "bers",
        "ren",
        "kle",
        "gel",
        "nor",
        "ppen",
        "rick",
        "rgel"
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)


def get_halfling_name():
    male_names = [
        "Alton",
        "Ander",
        "Cade",
        "Corrin",
        "Eldon",
        "Errich",
        "Finnan",
        "Garret",
        "Lindal",
        "Lyle",
        "Merric",
        "Milo",
        "Osborn",
        "Perrin",
        "Reed",
        "Roscoe",
        "Wellby"
    ]
    female_names = [
        "Andry",
        "Bree",
        "Callie",
        "Cora",
        "Euphemia",
        "Jillian",
        "Kithri",
        "Lavinia",
        "Lidda",
        "Merla",
        "Nedda",
        "Paela",
        "Portia",
        "Seraphina",
        "Shaena",
        "Trym",
        "Vani",
        "Verna"
    ]
    family_name_prefix = [
        "Brush",
        "Good",
        "Green",
        "High",
        "Hill",
        "Lea",
        "Tea",
        "Thorn",
        "Toss",
        "Under"
        "Bad",
        "Berry",
        "Orange",
        "Brown",
    ]
    family_name_suffix = [
        "gather",
        "barrel",
        "-hill",
        "bottle",
        "topple",
        "leaf",
        "bough",
        "cobble",
        "gage",
        "gallow",
        "basket",
        "meadow"
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)


def get_human_name():
    male_names = [
        "Abraham",
        "Adam",
        "Algamis",
        "Franklin",
        "Jack",
        "Jimmy",
        "John",
        "Joseph",
        "Indie"
        "Rolf",
        "Ronald",
        "Ryan"
        "Stan",
        "Stephan",
        "Steve",
        "Will",
    ]
    female_names = [
        "Ada",
        "Anna",
        "Annie",
        "Eva",
        "Eve",
        "Stephanie"
    ]
    family_name_prefix = [
        "Big",
        "Bour",
        "Con",
        "Hel",
        "Long",
        "Nei",
        "Neut",
        "Jack",
        "Roll",
        "Short",
        "Small",
        "Steve",
        "Wood",
    ]
    family_name_suffix = [
        "brooks",
        "dell",
        "man",
        "ne",
        "nell",
        "ron",
        "son",
        "wick",
        "worth"
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)


def get_elf_name():
    male_names = [
        "Adran",
        "Aelar",
        "Aramil",
        "Arannis",
        "Aust",
        "Beiro",
        "Berrian",
        "Carric",
        "Enialis",
        "Erdan",
        "Erevan",
        "Galinndan",
        "Hadarai",
        "Heian",
        "Himo",
        "Immeral",
        "Ivellios",
        "Laucian",
        "Mindartis",
        "Paelias",
        "Peren",
        "Quarion",
        "Riardon",
        "Rolen",
        "Soveliss",
        "Thamior",
        "Tharivol",
        "Theren",
        "Varis"
    ]
    female_names = [
        "Adrie",
        "Althaea",
        "Anastrianna",
        "Andraste",
        "Antinua",
        "Bethrynna",
        "Birel",
        "Caelynn",
        "Drusilia",
        "Enna",
        "Felosial",
        "Ielenia",
        "Jelenneth",
        "Keyleth",
        "Leshanna",
        "Lia",
        "Meriele",
        "Mialee",
        "Naivara",
        "Quelenna",
        "Quillathe",
        "Sariel",
        "Shanairra",
        "Shava",
        "Silaqui",
        "Theirastra",
        "Thia",
        "Vadania",
        "Valanthe",
        "Xanaphia"
    ]
    family_name_prefix = [
        "Amakiir",
        "Amastacia",
        "Galanodel",
        "Holimion",
        "Ilphelkiir",
        "Liadon",
        "Meliamne",
        "Naïlo",
        "Siannodel",
        "Xiloscient"
    ]
    family_name_suffix = [
        ""
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)


def get_half_elf_name():
    name = None
    name_type = random.randint(0, 1)
    if name_type == 0:
        name = get_human_name()
    elif name_type == 1:
        name = get_elf_name()
    return name


def get_dwarf_name():
    male_names = [
        "Adrik",
        "Alberich",
        "Baern",
        "Barendd",
        "Brottor",
        "Bruenor",
        "Dain",
        "Darrak",
        "Delg",
        "Eberk",
        "Einkil",
        "Fargrim",
        "Flint",
        "Gardain",
        "Harbek",
        "Kildrak",
        "Morgran",
        "Orsik",
        "Oskar",
        "Rangrim",
        "Rurik",
        "Taklinn",
        "Thoradin",
        "Thorin",
        "Tordek",
        "Traubon",
        "Travok",
        "Ulfgar",
        "Veit",
        "Vondal"
    ]
    female_names = [
        "Amber",
        "Artin",
        "Audhild",
        "Bardryn",
        "Dagnal",
        "Diesa",
        "Eldeth",
        "Falkrunn",
        "Finellen",
        "Gunnloda",
        "Gurdis",
        "Helja",
        "Hlin",
        "Kathra",
        "Kristryd",
        "Ilde",
        "Liftrasa",
        "Mardred",
        "Riswynn",
        "Sannl",
        "Torbera",
        "Torgga",
        "Vistra"
    ]
    family_name_prefix = [
        "Bal",
        "Battle",
        "Brawn",
        "Dan",
        "Fire",
        "Frost",
        "Gor",
        "Hol",
        "Iron",
        "Loderr",
        "Lut",
        "Rumna",
        "Stra",
        "Tor",
        "Un",
        "Bronze",
        "Iron",
        "Steel",
        "Copper",
        "Tin"
    ]
    family_name_suffix = [
        "gart",
        "heim",
        "hek",
        "fist",
        "keln",
        "unn",
        "gehr",
        "der",
        "beard",
        "forge",
        "kil",
        "anvil",
        "hammer",
        "derk",
        "axe",
        "pick"
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)

def get_tiefling_name():
    male_names = [
        "Akmenos",
        "Amnon",
        "Barakas",
        "Damakos",
        "Ekemon",
        "Iados",
        "Kairon",
        "Leucis",
        "Melech",
        "Mordai",
        "Morthos",
        "Pelaios",
        "Skamos",
        "Therai"
    ]
    female_names = [
        "Akta",
        "Anakis",
        "Bryseis",
        "Criella",
        "Damaia",
        "Ea",
        "Kallista",
        "Lerissa",
        "Makaria",
        "Nemeia",
        "Orianna",
        "Phelaia",
        "Rieta"
    ]
    family_name_prefix = [
        "Art",
        "Carrion",
        "Chant",
        "Creed",
        "Despair",
        "Excellence",
        "Fear",
        "Glory",
        "Hope",
        "Ideal",
        "Music",
        "Nowhere",
        "Open",
        "Poetry",
        "Quest",
        "Random",
        "Reverence",
        "Sorrow",
        "Temerity",
        "Torment",
        "Weary"
    ]
    family_name_suffix = [
        ""
    ]
    return name_builder(male_names, female_names, family_name_prefix, family_name_suffix)


def name_builder(male_names, female_names, family_name_prefix, family_name_suffix):
    name_parts = []
    if sex == "male":
        name_parts.append(male_names[random.randint(0, len(male_names)-1)])
    else:
        name_parts.append(female_names[random.randint(0, len(female_names)-1)])
    name_parts.append(family_name_prefix[random.randint(0, len(family_name_prefix)-1)])
    name_parts.append(family_name_suffix[random.randint(0, len(family_name_suffix)-1)])
    name = '{} {}{}'.format(name_parts[0], name_parts[1], name_parts[2])
    return name


def generate(race, char_sex):
    sex = char_sex
    name = None
    if race == "dragonborn":
        name = get_dragonborn_name()
    elif race == "dwarf":
        name = get_dwarf_name()
    elif race == "elf":
        name = get_elf_name()
    elif race == "gnome":
        name = get_gnome_name()
    elif race == "halfling":
        name = get_halfling_name()
    elif race == "half-elf":
        name = get_half_elf_name()
    elif race == "human":
        name = get_human_name()
    elif race == "half-orc":
        name = get_half_orc_name()
    elif race == "tiefling":
        name = get_tiefling_name()
    return name