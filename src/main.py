from tkinter import ACTIVE

import PySimpleGUI as gui
from character import *
import json

path = "../saved_characters.json"

char = None
recent = []
saved = []
saved_chars = []
try:
    saved = json.load(open(path, 'r'))
    for data in saved:
        character = Character(data)
        saved_chars.append(character)
except FileNotFoundError:
    print("Existing characters not found")
finally:
    print("")


# Load character object data
def load_object():
    char_data = char.get_data()
    update(char_data)


# Update function
def update(char_data):
    # Class and race details
    window['-name-'].update("Name: {}".format(char_data['name'].capitalize()))
    window['-class-'].update("Class: {}".format(char_data['class'].capitalize()))
    window['-race-'].update("Race: {}".format(char_data['race'].capitalize()))
    window['-size-'].update('  Size: {}'.format(char_data['size']))
    window['-speed-'].update('  Speed: {}ft'.format(char_data['speed']))

    # Attribute details
    attr = []
    for key, value in char_data['attributes'].items():
        attribute = '{}: {}; mod {:+}'.format(key, value[0], value[1])
        attr.append(attribute)

    window['-attr-'].update(attr)

    # class and racial features
    window['-race feat-'].update(char_data['features'][0])
    window['-class feat-'].update(char_data['features'][1])

    # storing character data
    window['-recent-'].update(recent)

    window['-cantrips-'].update(char_data['cantrips'])
    window['-spells-'].update(char_data['spells'])

    window['-proficiencies-'].update(char_data['proficiencies'])
    window['-health-'].update(char_data['health'])
    window['-equipment-'].update(char_data['equipment'])


def remove_char():
    for data in saved_chars:
        if char is data:
            saved_chars.remove(data)
    window['-saved-'].update(saved_chars)


# the character stat layout
stat_layout = [
    [gui.Text('Name: ', key='-name-', size=(20, 1), font='Helvetica 11')],
    [gui.Text('Class: ', key='-class-', size=(20, 1), font='Helvetica 11')],
    [gui.Text('Race: ', key='-race-', size=(20, 1), font='Helvetica 11')],
    [gui.Text('  Size: ', key='-size-', size=(20, 1), font='Helvetica 11')],
    [gui.Text('  Speed: ', key='-speed-', size=(20, 2), font='Helvetica 11')],

    [gui.Text('Attributes: ', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(22, 6), key="-attr-", no_scrollbar=True)],
    [
        gui.Button('Roll', enable_events=True, font='Helvetica 11'),
        gui.Button('Save', enable_events=True, font='Helvetica 11')
     ]
]

# The feature section layout
feature_layout = [
    [gui.Text('Racial Features: ', size=(25, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(25, 8), key="-race feat-", no_scrollbar=True)],
    [gui.Text('Class Features: ', size=(25, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(25, 8), key="-class feat-", no_scrollbar=True)],
]


# The character section layout
character_layout = [
    [gui.Text('Recent characters:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(24, 7), key="-recent-")],
    [gui.Text('Saved characters:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(24, 7), key="-saved-")],
    [gui.Button('Delete selected', enable_events=True, font='Helvetica 11', key='-delete-')]
]

# The character section layout
spell_layout = [
    [gui.Text('Note: Spellcasters only', size=(20, 1), font='Helvetica 11')],
    [gui.Text('Character cantrips:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(25, 7), key="-cantrips-")],
    [gui.Text('Character spells:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(25, 7), key="-spells-")],
]

proficiency_layout = [
    [gui.Text('Proficiencies:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(63, 4), key="-proficiencies-")],
]

health_layout = [
    [gui.Text('Health details:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(57, 4), key="-health-", no_scrollbar=True)],
]

equipment_layout = [
    [gui.Text('Equipment:', size=(20, 1), font='Helvetica 11')],
    [gui.Listbox(values=[], enable_events=True, size=(57, 4), key="-equipment-", no_scrollbar=True)],
]

# The general layout used in the application
layout = [
    [
        gui.Column(character_layout),
        gui.VSeperator(),
        gui.Column(stat_layout),
        gui.VSeperator(),
        gui.Column(feature_layout),
        gui.VSeperator(),
        gui.Column(spell_layout),
    ],
    [
        gui.Column(health_layout),
        gui.VSeperator(),
        gui.Column(proficiency_layout),
    ],
    [
        gui.Column(equipment_layout),
    ]
]


# Create the window
window = gui.Window('CharCreator', layout, size=(1000, 600))


# Event loop
running = True
while True:
    event, values = window.read(timeout=150)
    if running:
        running = False
        window['-saved-'].update(saved_chars)
    if event in (gui.WIN_CLOSED, 'Exit'):
        saved = []
        with open(path, 'w') as outfile:
            for character in saved_chars:
                saved.append(character.get_data())
            json.dump(saved, outfile, indent=4)
        break
    if event == 'Roll':
        char = Character()
        recent.append(char)
        load_object()
    if event == '-recent-':
        try:
            char = window['-recent-'].get()[0]
            load_object()
        except IndexError:
            print('out of list')
    if event == '-saved-':
        char = window['-saved-'].get()[0]
        load_object()
    if event == 'Save':
        saved_chars.append(char)
        window['-saved-'].update(saved_chars)
    if event == '-delete-':
        print("Attempting a character removal")
        char = window['-saved-'].get()[0]
        remove_char()
window.close()
